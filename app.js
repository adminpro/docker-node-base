var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send(process.env.VIRTUAL_HOST);
  console.log(process.env);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
})
